class BiDict(dict):
    """
    A Bi Directional Dictionary to maintain two way mapping
    """

    def __init__(self, *args, **kwargs):
        super(BiDict, self).__init__(*args, **kwargs)
        self.inverse = {}
        for key, value in self.items():
            self.inverse[value] = key

    def __setitem__(self, key, value):
        super(BiDict, self).__setitem__(key, value)
        self.inverse[value] = key

    def __delitem__(self, key):
        if self[key] in self.inverse:
            del self.inverse[self[key]]
        super(BiDict, self).__delitem__(key)


class SeatMap(BiDict):
    """
    Maintains Customer Name <-> Seat Allotted Mapping
    """
    pass


class TicketMap(BiDict):
    """
    Maintains Customer Name <-> Ticket Number Mapping
    """
    pass
