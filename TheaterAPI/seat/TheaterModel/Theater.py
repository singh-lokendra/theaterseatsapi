from typing import Dict, Union

from .utils import SeatMap
from .utils import TicketMap


class Theater:
    msg: str = "message"
    houseful: str = "HouseFul"
    seat_vacated: str = "Seat Vacated"
    seat_already_vacant: str = "Seat Already Vacant"
    seat_occupied: str = "Customer Already Present"
    valid_details: str = "Provide Valid Details"
    name: str = "name"
    ticket: str = "ticket"
    seat_no: str = "seat_no"

    def __init__(self, max_seats: int):
        """
        self._empty_seats: stores all the empty seats
        self._seat_map: customer Name <-> seat allotted Mapping
        self._ticket_map: customer Name <-> ticket number Mapping
        """
        self._empty_seats: set = set(range(1, max_seats + 1))
        self._seat_map = SeatMap()
        self._ticket_map = TicketMap()

    def occupy_seat(self, name: str, ticket_id: str) \
            -> Dict[str, Union[int, str]]:
        if not self._empty_seats:
            return {Theater.msg: Theater.houseful}

        if name in self._ticket_map or ticket_id in self._ticket_map.inverse:
            return {Theater.msg: Theater.seat_occupied}

        self._ticket_map[name] = ticket_id
        self._seat_map[name] = self._empty_seats.pop()
        return {Theater.seat_no: self._seat_map[name]}

    def vacate_seat(self, seat_no: int) -> Dict[str, str]:
        if seat_no in self._seat_map.inverse:
            name = self._seat_map.inverse[seat_no]
            del self._seat_map[name]
            del self._ticket_map[name]
            self._empty_seats.add(seat_no)
            return {Theater.msg: Theater.seat_vacated}
        else:
            return {Theater.msg: Theater.seat_already_vacant}

    def get_info(self, name: str = None, ticket: str = None,
                 seat_no: int = None) -> Dict[str, Union[str, int]]:
        try:
            if ticket:
                name = self._ticket_map.inverse[ticket]
            elif seat_no:
                name = self._seat_map.inverse[seat_no]
            elif not name:
                return {Theater.msg: Theater.valid_details}

            ticket = self._ticket_map[name]
            seat_no = self._seat_map[name]

            return {
                Theater.name: name,
                Theater.ticket: ticket,
                Theater.seat_no: seat_no,
            }
        except KeyError:
            return {Theater.msg: Theater.valid_details}
