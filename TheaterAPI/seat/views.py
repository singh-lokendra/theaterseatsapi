from uuid import UUID

from django.http.response import JsonResponse

from . import Theater
from . import theater


def occupy(request, name, ticket):
    response = theater.occupy_seat(name, ticket)
    return JsonResponse(response)


def vacate(request, seat_no):
    response = theater.vacate_seat(seat_no)
    return JsonResponse(response)


def get_info(request):
    name = request.GET.get('name', None)
    ticket = request.GET.get('ticket', None)
    seat_no = (request.GET.get('seat_no', None))
    try:
        if seat_no:
            seat_no = int(seat_no)
        if ticket:
            ticket = UUID(ticket)
    except ValueError:
        return JsonResponse({Theater.msg: Theater.valid_details})

    response = theater.get_info(name, ticket, seat_no)
    return JsonResponse(response)
