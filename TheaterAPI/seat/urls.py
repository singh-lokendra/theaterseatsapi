from django.urls import path

from .views import get_info
from .views import occupy
from .views import vacate

urlpatterns = [
    path('occupy/<str:name>/<uuid:ticket>/', occupy, name='occupy'),
    path('vacate/<int:seat_no>/', vacate, name='vacate'),
    path('get_info/', get_info, name='get_info'),
]
