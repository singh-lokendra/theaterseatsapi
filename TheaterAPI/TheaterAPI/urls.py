from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('seat/', include('seat.urls')),
    path('admin/', admin.site.urls),
]
